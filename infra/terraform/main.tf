provider "aws" {
  profile = "default"
  region  = var.region
}

#Search Ubuntu  20.04
data "aws_ami" "ubuntu" {
     owners      = ["099720109477"] # Ubuntu
     most_recent = true
     filter {
       name   = "name"
       values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]  
     }     
     filter {
       name   = "virtualization-type"
       values = ["hvm"]  
    }     
}

#Search zones in region
data "aws_availability_zones" "available" {}

#Security Group
resource "aws_security_group" "web-servers" {
  name        = "Dynamic SecurityGroup"
  description = "Allow inbound traffic"

dynamic "ingress" {
  for_each = var.allow_ports
  content {
    from_port        = ingress.value
    to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
   tags = merge(var.common_tags, { Name = "${var.common_tags["Enviroment"]} Dynamic SecurityGroup" })
 
}

#Configure Servers
resource "aws_launch_configuration" "web" {
  name_prefix     = "WebServer-LC"
  image_id        = data.aws_ami.ubuntu.id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.web-servers.id]
  user_data       = file("user_data.sh")
  key_name        = "server2"

  lifecycle {
    create_before_destroy = true
  }
}

#Instance 
resource "aws_autoscaling_group" "web" {
  name                 ="ASG-${aws_launch_configuration.web.name}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2 #два сервер готовы load balancer проверит
  vpc_zone_identifier  = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]   #в каких саб листах хапускать сервера
  health_check_type    = "ELB" # делает пинг на страничку если не работает убить. 
  #есть EC2 - проверка систем чеков. 
  load_balancers       = [aws_elb.web.name]  #
  
  dynamic "tag" {
    for_each = {
      Name  = "WeBServer ASG"
      Owner = "Alexey"
    }
    content {
        key                = tag.key
        value               = tag.value
        propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "web" {
  name               = "WebServer-HA-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web-servers.id]
  listener {
    lb_port             = 80
    lb_protocol         = "http"
    instance_port       = 80
    instance_protocol   = "http"
  }
  listener {
    lb_port            = 443
    lb_protocol        = "https"
    instance_port      = 80
    instance_protocol  = "http"
    ssl_certificate_id = var.ssl_certificate_id
  }
  listener {
    lb_port            = 8080
    lb_protocol        = "http"
    instance_port      = 8080
    instance_protocol  = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 30
    target              = "HTTP:80/"
    interval            = 40 
  }
  tags = merge(var.common_tags, { Name = "${var.common_tags["Enviroment"]} WebServer-Hihly-Available-ELB" })

}


resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}
resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

#add route53 zone
resource "aws_route53_zone" "route_zone" {
  name         = var.site_rote53
  tags         = merge(var.common_tags, { Name = "${var.common_tags["Enviroment"]} AWS-ROUTE53" })
}

#add record A www.exemple.com 
resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.route_zone.zone_id
  name    = "www.${var.site_rote53}"
  type    = "A"

  alias {
    name                   = aws_elb.web.dns_name
    zone_id                = aws_elb.web.zone_id
    evaluate_target_health = true
  }
}
# add record A exemple.com
resource "aws_route53_record" "without_www" {
  zone_id = aws_route53_zone.route_zone.zone_id
  name    = var.site_rote53
  type    = "A"

  alias {
    name                   = aws_elb.web.dns_name
    zone_id                = aws_elb.web.zone_id
    evaluate_target_health = true
  }
}