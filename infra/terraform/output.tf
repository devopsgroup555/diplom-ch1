output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}

#output "instance_ip_addr_pubic" {
#  value       = aws_launch_configuration.associate_public_ip_address
#  description = "The public IP address of the main server instance."
#}