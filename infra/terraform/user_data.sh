#!/bin/bash
sudo apt update -y
sudo apt install nginx -y
sudo chown ubuntu: /var/www/html/index.html
myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
cat <<EOF > /var/www/html/index.html
<html>
<h2>Build by Power of Terraform <font color="red">v1.5</font></h2><br>
<font color="green">Server PrivateIP: <font color="blue">$myip<br><br>
</html>
EOF
